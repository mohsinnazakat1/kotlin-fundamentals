package com.example.kotlinfundamentals

// single line comment 
/*mutli line comment in kotlin*/

// function implementation in kotlin
// fun main () {
//     print("Hello World")

// }


// fun main () {
//     val name = "Mohsin"
//     // string contcatination
//     print("Hello World " + name)

// }



// fun main () {
//     val first_name = "Mohsin" // val cannot be re-assigned (immutable)
//     var last_name = "Nazakat" // var can be re-assigned (mutable)

//     //use var when you belive something needs to be changed in that variable later 


// }

// kotlin is a typed language (you assign a value to variable and kotlin determins tha type)

// fun main () {
//     val first_name = "Mohsin" //String
//     var age = 23 // int 

//     //We also have other int type 
//     //Byte (8 bit)
//     //Short (16 bit)
//     //Int (32 bit)
//     //Long (64 bit)

//     // you can assign type like this 
//     var age: Byte = 23 // int 
//     var age: Short = 23 // Short
//     //...

    

// }


 